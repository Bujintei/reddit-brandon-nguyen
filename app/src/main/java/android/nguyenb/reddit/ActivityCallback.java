package android.nguyenb.reddit;

import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(Uri redditPostUri);
}
